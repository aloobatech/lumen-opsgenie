<?php

namespace Alooba\LumenOpsgenieAlert;


class Priority
{
    const ALERT = 'P2';
    const NOTICE = 'P5';

    private $priority;

    private function __construct(string $priority)
    {
        $this->priority = $priority;
    }


    public static function fromString($priority): ?Priority
    {
        if ($priority === Priority::ALERT) {
            return new Priority(Priority::ALERT);
        }
        if ($priority === Priority::NOTICE) {
            return new Priority(Priority::NOTICE);
        }
        return null;
    }

    /**
     * @return string
     */
    public function getPriority(): string
    {
        return $this->priority;
    }

    public function __toString(): string
    {
        return $this->priority;
    }


}