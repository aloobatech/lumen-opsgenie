<?php
namespace Alooba\LumenOpsgenieAlert;

use GuzzleHttp\Client;

class Alert
{
    /**
     * @var string
     */
    private $serviceName;
    /**
     * @var string
     */
    private $key;

    public function __construct(string $serviceName, string $key)
    {
        $this->serviceName = $serviceName;
        $this->key = $key;
    }

    public function send(\Throwable $error, Priority $priority = null)
    {
        if ($priority === null) {
            $priority = Priority::ALERT;
        }
        if ($error instanceof FrontendErrorException) {
            $priority = $error->getPriority();
            $this->serviceName = 'FRONTEND';
        }
        $client = new Client([
            'base_uri' => 'https://api.eu.opsgenie.com/v2/',
        ]);
        $client->post('alerts', [
            'headers' => [
                'Authorization' => 'GenieKey ' . $this->key,
                'Content-Type' =>  'application/json',
            ],
            'json' => [
                'message' => 'Error on ' . $this->serviceName,
                'description' => $this->formatDescription($error),
                'priority' => $priority,
                'alias' => $this->generateAlias($error),
            ],
        ]);
    }

    private function generateAlias(\Throwable $error): string
    {
        if ($error instanceof FrontendErrorException) {
            return md5($error->getMessage());
        }
        $file = $error->getFile();
        $line = $error->getLine();
        return md5("$file:$line");
    }

    private function formatDescription(\Throwable $error): string
    {
        if ($error instanceof FrontendErrorException) {
            $json = json_encode($error->getDetails(), JSON_PRETTY_PRINT);
            return $error->getMessage() . "\n" . $json;
        }
        return $error->getMessage() . "\n" .
            $error->getTraceAsString();
    }
}