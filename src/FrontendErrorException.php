<?php

namespace Alooba\LumenOpsgenieAlert;

use Exception;

class FrontendErrorException extends Exception
{
    protected $details;
    private $priority;

    public function __construct($message, Priority $priority, $details)
    {
        parent::__construct($message, 0, null);
        $this->details = $details;
        $this->priority = $priority;
    }

    public function getDetails()
    {
        return $this->details;
    }

    /**
     * @return Priority
     */
    public function getPriority(): string
    {
        return $this->priority->getPriority();
    }
}